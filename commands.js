const main = require('./index');
const fs = require('fs');
const path = require('fs');
const { JSDOM } = require('jsdom');

module.exports = [

    {
        name: 'get-characters [output file]',
        description: 'Get a list of all characters and their expressions',
        customHelp: '-o / --output <path> - Output the results to a file',

        // Return true if the command succeeded
        action: function(args, path) {

            // Check option -o
            var res = getOption(args, ['-o', '--output'], opt => {
                return fs.existsSync(path);
            });

            if (res) {
                args = res?.args;
                var outputPath = res?.output;
            }

            // Check if command is this one
            if (!args || args[0] !== 'get-characters') return false;

            // Parse the script
            const script = main.parseScriptHtml(path).window.document;
            const dialogues = script.body.querySelectorAll('.dialogue');

            // Encode as JSON
            const result = [];

            for (let line of dialogues) {
                //console.log(line.textContent);
                let expression = line.childNodes[2] ?
                    line.childNodes[2].textContent
                    .replace("(", "")
                    .replace(")", "")
                    : "neutral";

                result.push({
                    character: line.firstChild.textContent,
                    expression: expression,
                    text: line.childNodes[1].textContent,
                });

               // console.log(line.firstChild.textContent)
            }

            // Sort array to remove duplicates
            var filtered = [];
            for (let line of result) {
                if (!filtered.find(l => l.character === line.character && l.expression === line.expression)) {
                    filtered.push(line);
                }
            }


            // Create an object containing every character with its emotions
            const expressions = {};
            for (let line of filtered) {
                if (!expressions[line.character])
                    expressions[line.character] = {};

                expressions[line.character][line.expression] = 0;
            }


            // Count how many expressions for each character
            for (let char in expressions) {
                for (let exp in expressions[char]) {
                    expressions[char][exp] = result.filter(line => line.character === char && line.expression === exp).length;
                }
            }

            console.log(JSON.stringify(expressions, null, '\t'));

            // Write to output file if provided
            if (outputPath) {
                fs.writeFileSync(outputPath, JSON.stringify(expressions, null, "\t"));
                console.log("Successfully wrote output to " + outputPath);
            }

            return true;
        },
    },



    {
        name: "to-csv <output>",
        description: 'Convert to CSV',
        action: function(args, path) {
            // Check if command is this one
            if (!args || args[0] !== 'to-csv') return false;

            // Parse the script
            const script = main.parseScriptHtml(path).window.document;
            const res = [];
            var scenes = script.querySelectorAll('h2');

            fs.writeFileSync(".html.html", script.body.innerHTML)

            // Parse the dialogue
            for (let scene of scenes) {
                //console.log(scene.textContent.toLowerCase());
                const lines = getNextUntil(scene, 'h2');

                for (let line of lines) {
                    if (!line.matches(".dialogue"))
                        continue;

                    console.log(line.innerHTML);

                    let char = line.querySelector('h4');
                    let textElt = line.querySelector('p')
                    var text = textElt.textContent;
                    var expression = "neutral";

                    if (textElt.innerHTML.includes("<br")) {
                        const split = textElt.innerHTML.split('<');
                        const split2 = textElt.innerHTML.split('>');
                        expression = split[0];
                        text = split2[1];
                    }

                    res.push({scene: scene.textContent.toLowerCase(), character: char.textContent.toLowerCase(), expression: expression, text: text});
                }
            }

            // To CSV
            var row = 1;
            var data = ",Row Name,LEVEL_ID,Conversation_Name,Line_ID,Character_Name,Expression,Dialogue";
            var prevScene = "";
            for (let rec of res) {
                var lineId = 1;
                if (prevScene != rec.scene) {
                    lineId = 1;
                    prevScene = rec.scene;
                } else lineId++;

                data += `\n${row}, 1,` + `${rec.scene},` + `${lineId},` + `${rec.character},` + `${rec.expression},` + `${rec.text.replaceAll(",", "@")},`;
                row++
            }

            fs.writeFileSync(args[1], data);
            console.log('Finished writing to ' + path);
            return true;
        }
    }

];




function getOption(args, opt, predicate) {
    // Cast to array
    if (!Array.isArray(opt))
        opt = [opt];

    const optIndex = args.findIndex(arg => opt.find(o => o === arg));
    if (optIndex !== -1) {
        const outputArgs = args.splice(optIndex, 2);
        var output = outputArgs[1];

        if (!output || !predicate(output)) {
            console.log(`Invalid option specified after ${outputArgs[0]}`);
            return null;
        }

        return {
            output: output,
            args: args
        };
    }
}

var getNextUntil = function (elem, selector) {

    // Setup siblings array and get next sibling
    var siblings = [];
    var next = elem.nextElementSibling;

    // Loop through all siblings
    while (next) {

        // If the matching item is found, quit
        if (selector && next.matches(selector)) break;

        // Otherwise, push to array
        siblings.push(next);

        // Get the next sibling
        next = next.nextElementSibling

    }

    return siblings;

};
