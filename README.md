# Fountain Parser

## Installation

Download the .exe in the releases

## Requirements

* Node JS 16+
* NPM

## Setup

```bash
$ npm install
```

## Build

Execute build.bat or this command

```bash
$ pkg -t node16-win-x64 . --out-path ./build
```