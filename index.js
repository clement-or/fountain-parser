const fs = require("fs")
const fountain = require("fountain-js")
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const { program } = require("commander");
program.version("1.0.0");

const commands = require('./commands');

module.exports.main = function(args) {

    let path = args.splice(0, 1)[0];
    if (!path || !fs.existsSync(path.toString())) {
        console.log("You must provide a valid path. Usage: fountain-parser <path> <command>. Use fountain-parser help for more info.");
        return;
    }

    if (args[0] === 'help') {
        return displayHelp();
    }

    let success = false
    for (let c of commands) {
        try {
            if (c.action(args, path)) {
                success = true;
                break;
            }
        } catch (e) {
            console.log(e.message);
            success = true;
            break;
        }
    }

    if (!success) {
        console.log("The command you issued doesn't exist. Use fountain-parser help to get a list of commands");
    }
}


module.exports.parse = function(path) {
    console.log(`Parsing file ${path}`);

    const buffer = fs.readFileSync(path);
    const script = fountain.parse(buffer.toString());

    //fs.writeFileSync("./output.json", JSON.stringify(script));

    console.log("Finished parsing file " + path);
    return script;
}


module.exports.parseScriptHtml = function(path) {
    const script = module.exports.parse(path);
    if (!script) return;

    return new JSDOM(script.script_html);
}


function displayHelp(command=null) {

    if (command) {
        const found = commands.find(c => c.name === command);
        if (found) {
            const help = `
                ${found.name}
                ${found.description}
                ${found.customHelp}
            `;

            console.log(help);
            return;
        } else
            console.log(`Command ${command} doesn't exist, here is help: \n`);
    }

    let commandsHelps = "";
    for (let c in commands) {
        commandsHelps += `\n\t\t${c.name} - ${c.description}`;
    }

    const help = `
        Usage: fountain-parser <path> <command>
        
        Available commands:
            ${commandsHelps}
    `;

    console.log(help);
}